import { Component,ViewChild ,OnInit,ChangeDetectorRef} from '@angular/core';
import { ChatService } from './chat.service';
import { Observable,fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  @ViewChild('inputText') input;
  @ViewChild('messagesContainer') messagesContainer;

  public messages = [];
  public username = "";

  constructor(private chatService: ChatService,private ChangeDetectorRef:ChangeDetectorRef) {
    chatService.messages.subscribe(msg => {
      this.messages.push(msg.message);
      ChangeDetectorRef.detectChanges();
      this.scrollToBottom();    
    });
  }

  ngOnInit(){
    this.chatService.getLatestMessages();
  }

  loadMoreMessages(){
    this.chatService.getLatestMessages();
    this.scrollToTop()  
  }

  scrollToBottom(): void {
      this.messagesContainer.nativeElement.scrollTop = this.messagesContainer.nativeElement.scrollHeight;                
  }

  scrollToTop(): void {
    this.messagesContainer.nativeElement.scrollTop = 0;                
  }

  sendMsg() {
    this.chatService.messages.next({message:{user:this.username,message:this.input.nativeElement.value}});
    
  }
}