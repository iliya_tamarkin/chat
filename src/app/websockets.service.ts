import { Injectable } from '@angular/core';
import {Subject,Observable,Observer} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class WebsocketsService{
  constructor() {}

  private subject: Subject<MessageEvent>;

  public connect(url,protocol): Subject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url,protocol);
      console.log("Successfully connected: " + url);
    }
    return this.subject;
  }

  private create(url,protocol): Subject<MessageEvent> {
    let ws = new WebSocket(url,protocol);

    let observable = Observable.create((obs: Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs); // answer from the server
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });
    let observer = {
      next: (data: any) => { //use nest to send messages
        if (ws.readyState === WebSocket.OPEN) {
          if (typeof data == 'string'){
            data = {encrypted:data};
          } 
          ws.send(JSON.stringify(data));
          
        }
      }
    };
    return Subject.create(observer, observable); //create a websocket subject
  }
}