import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";
import { WebsocketsService } from "./websockets.service";
import { map } from "rxjs/operators";

const LAZY_MESSAGES_NUMBER = 5;


export interface Message {
  message:{
    user: string;
    message: string;
  }
  encrypted?:string;
}

const CHAT_URL = "ws://localhost:8080/";

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  public messages: Subject<any>;
  private decryptedMessages:Subject<Message>;

  private loadLatestMessagesIndex = localStorage.getItem('lastStorageIndex');

  constructor(wsService: WebsocketsService) {
    this.messages = <Subject<Message>>wsService.connect(CHAT_URL,'chat-protocol').pipe(map(
      (response: MessageEvent): Message => {
        let data = JSON.parse(response.data);
        
        // put the message data it the local storage
        if (data.encrypted){
          this.setMsgToLocalStorage(data.encrypted);
        }

        return data;
      }
    ));
  }

  getLatestMessages(){
    if (this.loadLatestMessagesIndex == '-1') return []; // no more messages in the local storage
    this.getDecryptedMessages(this.loadLatestMessagesIndex);  
  }


  getDecryptedMessages(localSKey){
    let messages = localStorage.getItem('messages'+localSKey);
    this.messages.next(messages);
  }


  setMsgToLocalStorage(msg){
    let lastIndex = localStorage.getItem('lastStorageIndex');
    if(lastIndex == undefined){ //first time
      localStorage.setItem('lastStorageIndex','0');
      lastIndex = '0';
      localStorage.setItem('messages'+lastIndex, JSON.stringify([]));
    }

    const currentMessagesStr = localStorage.getItem('messages'+lastIndex);
    const currentMessages = JSON.parse(currentMessagesStr);

    if (currentMessages.length > LAZY_MESSAGES_NUMBER){ //if filled the bucket create another one and update the index
      lastIndex = JSON.stringify(+lastIndex + 1);
      localStorage.setItem('lastStorageIndex',lastIndex);
      localStorage.setItem('messages'+lastIndex,  JSON.stringify([msg]));
    } else{

      currentMessages.push(msg);
      localStorage.setItem('messages'+lastIndex,  JSON.stringify(currentMessages));
    }
    
  }

}