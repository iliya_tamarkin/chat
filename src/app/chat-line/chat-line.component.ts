import { Component, OnInit ,Input} from '@angular/core';

@Component({
  selector: 'chat-line',
  templateUrl: './chat-line.component.html',
  styleUrls: ['./chat-line.component.css']
})
export class ChatLineComponent {
  @Input() message;
  constructor() { }
}
