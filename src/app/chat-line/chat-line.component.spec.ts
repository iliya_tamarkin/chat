import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatLineComponent } from './chat-line.component';

describe('ChatLineComponent', () => {
  let component: ChatLineComponent;
  let fixture: ComponentFixture<ChatLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
